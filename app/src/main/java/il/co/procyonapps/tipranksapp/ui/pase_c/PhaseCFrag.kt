package il.co.procyonapps.tipranksapp.ui.pase_c

import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.tipranksapp.MainActivity
import il.co.procyonapps.tipranksapp.R
import il.co.procyonapps.tipranksapp.network.StockResponse
import il.co.procyonapps.tipranksapp.ui.MainViewModel
import il.co.procyonapps.tipranksapp.ui.StocksAdapter
import il.co.procyonapps.tipranksapp.ui.main.PhaseAFragment
import kotlinx.android.synthetic.main.active_search_frag.*
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment.rvStockList

class PhaseCFrag : Fragment() {

    companion object {
        fun newInstance() = PhaseCFrag()
    }

    private val viewModel: MainViewModel by lazy { (activity as MainActivity).viewModel }

    private val adapter by lazy {
        StocksAdapter().apply {
            onLabelClick = labelClickListener
            onUidClick = uidClickListener
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.active_search_frag, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvStockList.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = this@PhaseCFrag.adapter


        }

        etSearch.filters = arrayOf(searchFilter)

        viewModel.status.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show();
            }
        })


    }

    private val labelClickListener: (String) -> Unit = {
        showAlert("uid: $it")
    }

    private val uidClickListener: (String, Int) -> Unit = { label, position ->
        showAlert("position: ${position + 1}\n" +
                "label: $label")
    }

    private val searchFilter =
        InputFilter { source, start, end, dest, dstart, dend ->
            Log.d("PhaseCFrag", "text typed: $source")

            if (source.length >= 2) {

                viewModel.getStocksBySearch(source.toString()).observe(
                    viewLifecycleOwner,
                    resultsObserver
                )
            }
            source
        }

    private val resultsObserver = Observer<List<StockResponse.StockItem>> {
        Log.d("PhaseCFrag", "search results: $it")
        adapter.stockItems = it
    }

    fun showAlert(text: String){
        AlertDialog.Builder(requireContext())
            .setMessage(text)
            .create()
            .show()
    }


}