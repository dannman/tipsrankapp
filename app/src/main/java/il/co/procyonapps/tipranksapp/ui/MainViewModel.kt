package il.co.procyonapps.tipranksapp.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import il.co.procyonapps.MyApp
import il.co.procyonapps.tipranksapp.data.ControlledRunner
import il.co.procyonapps.tipranksapp.network.StockResponse
import java.lang.Exception

class MainViewModel(val app: Application) : AndroidViewModel(app) {

    val controlledRunner = ControlledRunner<List<StockResponse.StockItem>>()

    val status = MutableLiveData("")


    fun getStaticList(): LiveData<List<StockResponse.StockItem>> = liveData<List<StockResponse.StockItem>> {
        try {
            val response = (app as MyApp).stocksApi.queryStocks("M")

            val sortedResponse = response.sortedBy { it.label }

            emit(sortedResponse)
        } catch (e: Exception){
            Log.e("MainViewModel", "networking error", e)
        }

    }

    fun getStocksBySearch(query: String): LiveData<List<StockResponse.StockItem>> = liveData {

        try {
            val current = controlledRunner.cancelPreviousThenRun {

                val response = (app as MyApp).stocksApi.queryStocks(query)

                val sortedResponse = response.sortedBy { it.label }

                sortedResponse

            }

            if (current.isEmpty()) {
                status.postValue("no results found!")
            }

            emit(current)
        } catch (e: Exception){
            status.postValue("error reading data from API")
            e.printStackTrace()
        }


    }


}
