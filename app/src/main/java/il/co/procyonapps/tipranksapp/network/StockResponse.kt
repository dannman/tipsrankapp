package il.co.procyonapps.tipranksapp.network


import com.google.gson.annotations.SerializedName

class StockResponse : ArrayList<StockResponse.StockItem>(){
    data class StockItem(
        @SerializedName("category") val category: String = "",
        @SerializedName("label") val label: String = "",
        @SerializedName("ticker") val ticker: String = "null",
        @SerializedName("uid") val uid: String = "",
        @SerializedName("value") val value: String = ""
    )
}