package il.co.procyonapps.tipranksapp.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.tipranksapp.MainActivity
import il.co.procyonapps.tipranksapp.R
import il.co.procyonapps.tipranksapp.network.StockResponse
import il.co.procyonapps.tipranksapp.ui.MainViewModel
import il.co.procyonapps.tipranksapp.ui.StocksAdapter
import kotlinx.android.synthetic.main.main_fragment.*

class PhaseAFragment : Fragment() {

    companion object {
        fun newInstance() = PhaseAFragment()
    }

    private val viewModel: MainViewModel by lazy { (activity as MainActivity).viewModel }

    private val adapter by lazy { StocksAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvStockList.apply{
            layoutManager = LinearLayoutManager(context)
            adapter = this@PhaseAFragment.adapter
        }

        viewModel.getStaticList().observe(viewLifecycleOwner, Observer<List<StockResponse.StockItem>>{
            adapter.stockItems = it

        })

    }

}
