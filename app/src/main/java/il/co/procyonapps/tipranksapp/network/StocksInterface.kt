package il.co.procyonapps.tipranksapp.network

import retrofit2.http.GET
import retrofit2.http.Query

interface StocksInterface {

    @GET("/api/Autocomplete/GetAutocomplete/")
    suspend fun queryStocks(@Query("name") query: String): StockResponse
}