package il.co.procyonapps.tipranksapp.ui

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import il.co.procyonapps.tipranksapp.R
import il.co.procyonapps.tipranksapp.network.StockResponse
import kotlinx.android.synthetic.main.list_item.view.*
import kotlin.properties.Delegates
import kotlin.random.Random

class StocksAdapter : RecyclerView.Adapter<StocksAdapter.ItemHolder>() {

    var stockItems: List<StockResponse.StockItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onLabelClick: ((String) -> Unit)? = null

    var onUidClick:((String, Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ItemHolder(view)
    }

    override fun getItemCount(): Int {
        return stockItems.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(stockItems[position])
    }

    inner class ItemHolder(val myView: View) : RecyclerView.ViewHolder(myView) {
        fun bind(item: StockResponse.StockItem) {
            myView.tvLabel.text = item.label
            myView.tvTicker.text = "ticker: ${item.ticker}"
            myView.tvUid.text = "uid: ${item.uid}"
            myView.tvValue.text = "value: ${item.value}"
            myView.tvCategory.text = "category: ${item.category}"

            myView.tvLabel.setOnClickListener {
                onLabelClick?.let{
                    it.invoke(item.uid)
                }
            }

            val position  = stockItems.indexOf(item)

            myView.tvUid.setOnClickListener {
                onUidClick?.let {
                    it.invoke(item.label, position)
                }
            }

            myView.setOnClickListener {
                val red = Random.nextInt(0, 255)
                val green = Random.nextInt(0, 255)
                val blue = Random.nextInt(0, 255)
                val color = Color.rgb(red, green, blue)


                myView.root.setCardBackgroundColor(color)
            }
        }


    }
}