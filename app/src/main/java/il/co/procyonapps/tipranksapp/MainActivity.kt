package il.co.procyonapps.tipranksapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import il.co.procyonapps.tipranksapp.ui.MainViewModel
import il.co.procyonapps.tipranksapp.ui.main.PhaseAFragment
import il.co.procyonapps.tipranksapp.ui.pase_c.PhaseCFrag

class MainActivity : AppCompatActivity() {

    val viewModel: MainViewModel by lazy { ViewModelProvider
        .AndroidViewModelFactory.getInstance(application)
        .create(MainViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, PhaseCFrag.newInstance())
                    .commitNow()
        }
    }
}
