package il.co.procyonapps

import android.app.Application
import il.co.procyonapps.tipranksapp.network.StocksInterface
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyApp: Application() {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://trautocomplete.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val stocksApi by lazy {


        retrofit.create(StocksInterface::class.java)
    }
}